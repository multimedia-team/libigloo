/* Copyright (C) 2021       Marvin Scholz <epirat07@gmail.com>
 * Copyright (C) 2021       Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>

#include <igloo/tap.h>
#include <igloo/igloo.h>
#include <igloo/error.h>

static void test_direct(void)
{
    igloo_ro_t igloo_instance;

    igloo_tap_test_success("igloo_initialize", igloo_initialize(&igloo_instance));

    igloo_tap_dump_ro_variable(igloo_instance);

    // Ensure instance is valid
    igloo_tap_test_success("igloo_instance_validate", igloo_instance_validate(igloo_instance));

    // Ensure instance is not null
    igloo_tap_test("not null", !igloo_ro_is_null(igloo_instance));

    igloo_tap_test_success("unref", igloo_ro_unref(&igloo_instance));

    // Verify instance is null now
    igloo_tap_test("is null", igloo_ro_is_null(igloo_instance));
}

static void test_by_group(void)
{
    igloo_ro_t instance;
    igloo_ro_object_group_t * group;

    igloo_tap_test_success("igloo_initialize", igloo_initialize(&instance));

    igloo_tap_test_success("igloo_ro_get_object_group", igloo_ro_get_object_group(instance, &group));

    igloo_tap_test_success("unref", igloo_ro_unref(&instance));
    igloo_tap_dump_ro_variable(group);
    igloo_tap_test_success("unref", igloo_ro_unref(&group));
}


static void test_by_object(void)
{
    igloo_ro_t instance;
    igloo_ro_full_t * full;

    igloo_tap_test_success("igloo_initialize", igloo_initialize(&instance));

    igloo_tap_test_success("igloo_ro_new", igloo_ro_new(&full, igloo_ro_full_t, instance));

    igloo_tap_test_success("unref", igloo_ro_unref(&instance));
    igloo_tap_dump_ro_variable(full);
    igloo_tap_test_success("unref", igloo_ro_unref(&full));
}

static void test_by_extra_group(void)
{
    igloo_ro_t instance;
    igloo_ro_object_group_t * group;

    igloo_tap_test_success("igloo_initialize", igloo_initialize(&instance));

    igloo_tap_test_success("igloo_ro_new", igloo_ro_new(&group, igloo_ro_object_group_t, instance));

    igloo_tap_test_success("unref", igloo_ro_unref(&instance));
    igloo_tap_dump_ro_variable(group);
    igloo_tap_test_success("unref", igloo_ro_unref(&group));
}

static void test_by_extra_group_and_object(void)
{
    igloo_ro_t instance;
    igloo_ro_object_group_t * group;
    igloo_ro_full_t * full;

    igloo_tap_test_success("igloo_initialize", igloo_initialize(&instance));

    igloo_tap_test_success("igloo_ro_new", igloo_ro_new(&group, igloo_ro_object_group_t, instance));

    igloo_tap_test_success("unref", igloo_ro_unref(&instance));

    igloo_tap_dump_ro_variable(group);
    igloo_tap_test_success("igloo_ro_new", igloo_ro_new(&full, igloo_ro_full_t, group));
    igloo_tap_test_success("unref", igloo_ro_unref(&group));

    igloo_tap_dump_ro_variable(full);
    igloo_tap_test_success("unref", igloo_ro_unref(&full));
}


int main(void)
{

    igloo_tap_init();
    igloo_tap_exit_on(igloo_TAP_EXIT_ON_FIN, NULL);
    igloo_tap_group_run("direct", test_direct);
    igloo_tap_group_run("by group", test_by_group);
    igloo_tap_group_run("by object", test_by_object);
    igloo_tap_group_run("by extra group", test_by_extra_group);
    igloo_tap_group_run("by extra group and object", test_by_extra_group_and_object);
    igloo_tap_fin();

    return EXIT_FAILURE; // return failure as we should never reach this point!
}
