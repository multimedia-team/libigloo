/* Icecast
 *
 * This program is distributed under the GNU General Public License, version 2.
 * A copy of this license is included with this source.
 *
 * Copyright 2021,      Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>,
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <igloo/tap.h>
#include <igloo/igloo.h>
#include <igloo/error.h>
#include <igloo/prng.h>


static igloo_ro_t g_instance;

static void dump(void)
{
    char *buf;

    if (igloo_ro_stringify(g_instance, &buf, igloo_RO_SY_DEFAULT) != igloo_ERROR_NONE)
        return;

    igloo_tap_diagnostic(buf);
    free(buf);
}

int main (void) {
    ssize_t res;
    size_t iter;
    unsigned char buf[16];

    igloo_tap_init();

    igloo_tap_test_success("igloo_initialize", igloo_initialize(&g_instance));
    if (igloo_ro_is_null(g_instance)) {
        igloo_tap_bail_out("Can not get an instance");
        return 1;
    }

    dump();
    igloo_tap_test_success("igloo_prng_auto_reseed", igloo_prng_auto_reseed(g_instance, igloo_PRNG_FLAG_NONE));

    dump();
    memset(buf, 0, sizeof(buf));
    igloo_tap_test_success("igloo_prng_write", igloo_prng_write(g_instance, buf, sizeof(buf), sizeof(buf)*8, igloo_PRNG_FLAG_NONE));

    for (iter = 0; iter < 16; iter++) {
        dump();
        res = igloo_prng_read(g_instance, buf, sizeof(buf), igloo_PRNG_FLAG_NONE);
        igloo_tap_test("igloo_prng_read", res == (ssize_t)sizeof(buf));
    }

    igloo_tap_test_success("unref instance", igloo_ro_unref(&g_instance));

    igloo_tap_fin();

    return 0;
}
