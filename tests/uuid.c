/* Copyright (C) 2022       Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>

#include <igloo/tap.h>
#include <igloo/igloo.h>
#include <igloo/uuid.h>
#include <igloo/sp.h>

static igloo_ro_t g_instance;

static void check_uuid(const char *ref)
{
    size_t len = strlen(ref);
    igloo_tap_test("strlen(ref) == 36", len == 36);

    igloo_tap_diagnostic("Ref is:");
    igloo_tap_diagnostic(ref);

    if (len == 36) {
        igloo_tap_test("ref[8] == '-' && ref[13] == '-' && ref[18] == '-' && ref[23] == '-'", ref[8] == '-' && ref[13] == '-' && ref[18] == '-' && ref[23] == '-');
    }
}

static void group_sp(void)
{
    const char *ref = NULL;

    igloo_tap_test_success("igloo_uuid_new_random_sp", igloo_uuid_new_random_sp(&ref, g_instance));
    igloo_tap_test("ref != NULL", ref != NULL);
    if (ref) {
        check_uuid(ref);
        igloo_tap_test_success("igloo_sp_unref", igloo_sp_unref(&ref, g_instance));
        igloo_tap_test("ref == NULL", ref == NULL);
    }
}

static void group_cstr(void)
{
    char *ref = NULL;

    igloo_tap_test_success("igloo_uuid_new_random_cstr", igloo_uuid_new_random_cstr(&ref, g_instance));
    igloo_tap_test("ref != NULL", ref != NULL);
    if (ref) {
        check_uuid(ref);
        free(ref);
    }
}

int main (void)
{
    igloo_tap_init();
    igloo_tap_exit_on(igloo_TAP_EXIT_ON_FIN, NULL);
    igloo_tap_test_success("igloo_initialize", igloo_initialize(&g_instance));
    igloo_tap_group_run("sp", group_sp);
    igloo_tap_group_run("cstr", group_cstr);
    igloo_tap_test_success("unref instance", igloo_ro_unref(&g_instance));
    igloo_tap_fin();

    return EXIT_FAILURE; // return failure as we should never reach this point!
}
