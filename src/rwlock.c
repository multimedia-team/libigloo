/* Copyright (C) 2020       Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <igloo/rwlock.h>

void igloo_rwlock_init(igloo_rwlock_t *lock)
{
    memset(lock, 0, sizeof(*lock));
    pthread_rwlock_init(&(lock->lock), NULL);
}

void igloo_rwlock_destroy(igloo_rwlock_t *lock)
{
    pthread_rwlock_destroy(&(lock->lock));
    memset(lock, 0, sizeof(*lock));
}

void igloo_rwlock_rlock(igloo_rwlock_t *lock)
{
    pthread_rwlock_rdlock(&(lock->lock));
}

void igloo_rwlock_wlock(igloo_rwlock_t *lock)
{
    if (lock->wlockc && pthread_equal(lock->writer, pthread_self())) {
        lock->wlockc++;
        return;
    }

    pthread_rwlock_wrlock(&(lock->lock));
    lock->writer = pthread_self();
    lock->wlockc++;
}

void igloo_rwlock_unlock(igloo_rwlock_t *lock)
{
    if (lock->wlockc && pthread_equal(lock->writer, pthread_self())) {
        lock->wlockc--;
        if (lock->wlockc)
            return;
    }
    pthread_rwlock_unlock(&(lock->lock));
}
