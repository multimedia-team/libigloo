/* Copyright (C) 2020       Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>

#include <igloo/list.h>
#include <igloo/error.h>

struct igloo_list_tag {
    igloo_ro_full_t __parent; 
    size_t offset;
    size_t fill; // count = fill - offset
    size_t length;
    igloo_ro_t *elements;
};

static igloo_error_t igloo_list_new(igloo_ro_t self, const igloo_ro_type_t *type, va_list ap);
static void igloo_list_free(igloo_ro_t self);

igloo_RO_PUBLIC_TYPE(igloo_list_t, igloo_ro_full_t,
        igloo_RO_TYPEDECL_NEW(igloo_list_new),
        igloo_RO_TYPEDECL_FREE(igloo_list_free)
        );

static igloo_error_t igloo_list_new(igloo_ro_t self, const igloo_ro_type_t *type, va_list ap)
{
    (void)self, (void)type, (void)ap;
    return igloo_ERROR_NONE;
}

static void igloo_list_free(igloo_ro_t self)
{
    igloo_list_t *list = igloo_ro_to_type(self, igloo_list_t);

    igloo_list_clear(list);

    /* free all memory */
    free(list->elements);
    list->length = 0;
}

igloo_error_t igloo_list_count(igloo_list_t *list, size_t *count)
{
    if (!igloo_ro_is_valid(list, igloo_list_t) || !count)
        return igloo_ERROR_FAULT;

    *count = list->fill - list->offset;

    return igloo_ERROR_NONE;
}

static inline void igloo_list_preallocate__realign(igloo_list_t *list)
{   
    size_t new_len;

    if (!list->offset)
        return;

    new_len = list->fill - list->offset;
    memmove(list->elements, list->elements + list->offset, new_len*sizeof(*list->elements));
    list->offset = 0;
    list->fill = new_len;
}

igloo_error_t igloo_list_preallocate__raw(igloo_list_t *list, size_t request, bool shrink)
{
    size_t new_len;

    if (!igloo_ro_is_valid(list, igloo_list_t))
        return igloo_ERROR_FAULT;

    new_len = (list->fill - list->offset) + request;

    if (new_len > list->length || (shrink && (new_len + 32) < list->length)) {
        igloo_ro_t *n;

        /* grow by at least 8 elements */
        if (new_len > list->length && (new_len - list->length) < 8)
            new_len = list->length + 8;

        igloo_list_preallocate__realign(list);

        n = realloc(list->elements, new_len*sizeof(*list->elements));
        if (!n)
            return igloo_ERROR_NOMEM;

        list->elements = n;
        list->length = new_len;
    }

    return igloo_ERROR_NONE;
}

igloo_error_t igloo_list_clear(igloo_list_t *list)
{
    size_t i;

    if (!igloo_ro_is_valid(list, igloo_list_t))
        return igloo_ERROR_FAULT;

    for (i = list->offset; i < list->fill; i++)
        igloo_ro_unref(&(list->elements[i]));

    list->offset = 0;
    list->fill = 0;

    igloo_list_preallocate__raw(list, 0, true);

    return igloo_ERROR_NONE;
}

igloo_error_t igloo_list_preallocate(igloo_list_t *list, size_t request)
{
    return igloo_list_preallocate__raw(list, request, true);
}

igloo_error_t igloo_list_push(igloo_list_t *list, igloo_ro_t element)
{
    igloo_error_t error;

    if (!igloo_ro_is_valid(list, igloo_list_t) || igloo_ro_is_null(element))
        return igloo_ERROR_FAULT;

    igloo_list_preallocate__raw(list, 1, false);

    if (list->fill == list->length)
        return igloo_ERROR_NOMEM;

    error = igloo_ro_ref_transparent(element, &(list->elements[list->fill]));
    if (error != igloo_ERROR_NONE)
        return error;

    list->fill++;

    return igloo_ERROR_NONE;
}

igloo_error_t igloo_list_unshift(igloo_list_t *list, igloo_ro_t element)
{
    igloo_error_t error;

    if (!igloo_ro_is_valid(list, igloo_list_t) || igloo_ro_is_null(element))
        return igloo_ERROR_FAULT;

    if (list->offset == list->fill)
        return igloo_list_push(list, element);

    if (!list->offset) {
        igloo_list_preallocate__raw(list, 1, false);

        if (list->fill == list->length)
            return igloo_ERROR_NOMEM;

        memmove(list->elements + 1, list->elements, sizeof(*list->elements)*list->fill);
        list->offset++;
        list->fill++;
    }

    if (!list->offset)
        return igloo_ERROR_NOMEM;

    error = igloo_ro_ref_transparent(element, &(list->elements[list->offset - 1]));
    if (error != igloo_ERROR_NONE)
        return error;

    list->offset--;

    return igloo_ERROR_NONE;
}

igloo_error_t igloo_list_shift(igloo_list_t *list, igloo_ro_t *element)
{
    if (!igloo_ro_is_valid(list, igloo_list_t) || !element)
        return igloo_ERROR_FAULT;

    if (list->offset == list->fill)
        return igloo_ERROR_NOENT;

    *element = list->elements[list->offset++];

    igloo_list_preallocate__raw(list, 0, true);

    return igloo_ERROR_NONE;
}

igloo_error_t igloo_list_pop(igloo_list_t *list, igloo_ro_t *element)
{
    if (!igloo_ro_is_valid(list, igloo_list_t) || !element)
        return igloo_ERROR_FAULT;

    if (list->offset == list->fill)
        return igloo_ERROR_NOENT;

    *element = list->elements[--list->fill];

    igloo_list_preallocate__raw(list, 0, true);

    return igloo_ERROR_NONE;
}

igloo_error_t igloo_list_merge(igloo_list_t *list, igloo_list_t *elements)
{
    size_t i;

    if (!igloo_ro_is_valid(list, igloo_list_t) || !igloo_ro_is_valid(elements, igloo_list_t))
        return igloo_ERROR_FAULT;

    if (elements->fill == elements->offset)
        return igloo_ERROR_NONE;

    igloo_list_preallocate__raw(list, elements->fill - elements->offset, false);

    if ((list->length - list->fill) < (elements->fill - elements->offset))
        return igloo_ERROR_NOMEM;

    for (i = elements->offset; i < elements->fill; i++) {
        igloo_error_t error = igloo_ro_ref_transparent(elements->elements[i], &(list->elements[list->fill]));
        if (error != igloo_ERROR_NONE)
            return error;

        list->fill++;
    }

    return igloo_ERROR_NONE;
}

igloo_error_t igloo_list_remove(igloo_list_t *list, igloo_ro_t element, bool equal)
{
    size_t i;

    if (!igloo_ro_is_valid(list, igloo_list_t) || igloo_ro_is_null(element))
        return igloo_ERROR_FAULT;

    if (equal)
        return igloo_ERROR_NOSYS;

    for (i = list->offset; i < list->fill; i++) {
        if (igloo_ro_is_same(list->elements[i], element)) {
            igloo_ro_unref(&(list->elements[i]));
            memmove(list->elements + i, list->elements + i + 1, (list->fill - i - 1)*sizeof(*list->elements));
            list->fill--;
            return igloo_ERROR_NONE;
        }
    }

    return igloo_ERROR_NOENT;
}
