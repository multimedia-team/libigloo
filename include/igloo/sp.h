/* Copyright (C) 2020       Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef _LIBIGLOO__CS_H_
#define _LIBIGLOO__CS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <igloo/types.h>

/* igloo_sp_ref() creates a new immutable reference to a immutable C string.
 *
 * igloo_sp_ref() creates a new reference by any mean it sees fit, including
 * creating a copy and deduplication.
 * The newly created reference will stay valid until igloo_sp_unref() is called,
 * that is even if the string it is created from becomes invalid.
 *
 * NOTE:
 *  This has been deprecated! Use igloo_sp_replace().
 *
 * Parameters:
 *  str
 *      The string to create a reference to. This can be a reference itself.
 *  ref
 *      Pointer to where to store the newly created reference.
 *  instance
 *      The instance to use. This instance must stay valid until igloo_sp_unref()
 *      is called on the newly created reference.
 * Returns:
 *  The error code for the operation.
 */
igloo_error_t igloo_sp_ref(const char *str, const char **ref, igloo_ro_t instance) igloo_ATTR_F_WARN_UNUSED_RESULT igloo_ATTR_F_DEPRECATED("Use igloo_sp_replace()");

/* igloo_sp_replace() creates a new immutable reference to a immutable C string.
 *
 * igloo_sp_replace() creates a new reference by any mean it sees fit, including
 * creating a copy and deduplication.
 * The newly created reference will stay valid until igloo_sp_unref() is called,
 * that is even if the string it is created from becomes invalid.
 *
 * Parameters:
 *  str
 *      The string to create a reference to. This can be a reference itself.
 *      If NULL *ref will also be set to NULL.
 *  ref
 *      Pointer to where to store the newly created reference.
 *      If *ref is not NULL igloo_sp_unref() will be called on it.
 *      This happens in a safe manner, so that igloo_sp_unref() is only called
 *      if this returns igloo_ERROR_NONE.
 *  instance
 *      The instance to use. This instance must stay valid until igloo_sp_unref()
 *      is called on the newly created reference.
 * Returns:
 *  The error code for the operation.
 */
igloo_error_t igloo_sp_replace(const char *str, const char **ref, igloo_ro_t instance) igloo_ATTR_F_WARN_UNUSED_RESULT;

/* Frees a reference to a string.
 * Parameters:
 *  ref
 *      Pointer to the reference as created by igloo_sp_ref().
 *  instance
 *      The instance used for igloo_sp_ref().
 * Returns:
 *  The error code for the operation.
 */
igloo_error_t igloo_sp_unref(const char **ref, igloo_ro_t instance) igloo_ATTR_F_WARN_UNUSED_RESULT;

#ifdef __cplusplus
}
#endif

#endif /* ! _LIBIGLOO__CS_H_ */
