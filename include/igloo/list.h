/* Copyright (C) 2020       Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef _LIBIGLOO__LIST_H_
#define _LIBIGLOO__LIST_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <igloo/ro.h>

/* About thread safety:
 * This set of functions is intentinally not thread safe unless marked otherwise.
 */

igloo_RO_FORWARD_TYPE(igloo_list_t);

/* Gets the currently count of elements */
igloo_error_t igloo_list_count(igloo_list_t *list, size_t *count) igloo_ATTR_F_WARN_UNUSED_RESULT;
/* Clear a list (remove all elements). */
igloo_error_t igloo_list_clear(igloo_list_t *list);
/* Preallocate space for later mass-adding of elements. */
igloo_error_t igloo_list_preallocate(igloo_list_t *list, size_t request);
/* Add an element at the end of the list. */
igloo_error_t igloo_list_push(igloo_list_t *list, igloo_ro_t element);
/* Add an element at the begin of a list. */
igloo_error_t igloo_list_unshift(igloo_list_t *list, igloo_ro_t element);
/* Get and remove the first element from the list. */
igloo_error_t igloo_list_shift(igloo_list_t *list, igloo_ro_t *element) igloo_ATTR_F_WARN_UNUSED_RESULT;
/* Get and remove the last element from the list. */
igloo_error_t igloo_list_pop(igloo_list_t *list, igloo_ro_t *element) igloo_ATTR_F_WARN_UNUSED_RESULT;

/* Merge the content of the list elements into the list list. The list elements is not changed. */
igloo_error_t igloo_list_merge(igloo_list_t *list, igloo_list_t *elements) igloo_ATTR_F_WARN_UNUSED_RESULT;

/* Remove a single element from the list.
 *
 * equal must be false.
 *
 * Note: This is not very efficient as the list is first searched for the element
 *       and then reorganized so there is no gap.
 */
igloo_error_t igloo_list_remove(igloo_list_t *list, igloo_ro_t element, bool equal);


#ifdef __cplusplus
}
#endif

#endif /* ! _LIBIGLOO__LIST_H_ */
