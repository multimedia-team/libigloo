/* Copyright (C) 2020-2022  Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef _LIBIGLOO__CS_H_
#define _LIBIGLOO__CS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <igloo/types.h>

/* All igloo_cs_*() functions MAY depend locale settings */

/* Replaces a string in a safe way.
 *
 * This function replaces the string *dst with src.
 * It calls free(3) on *dst to free the string if it is non-NULL.
 * To copy the string strdup(3) is used.
 *
 * If memory can not be allocated *dst is kept unchanged.
 */
igloo_error_t igloo_cs_replace(const char *src, char **dst) igloo_ATTR_F_WARN_UNUSED_RESULT;

/* Skips spaces in *str. This uses the same definition as isspace(3). */
igloo_error_t igloo_cs_skip_spaces(const char **str);

/* Converts strings to bools, ints, or unsigned ints.
 *
 * Range checking is performed.
 * Bools support at least the values "true", "false", "yes", "no", "on", "off", "1", and "0".
 * Other values may be supported depending on the locale.
 */
igloo_error_t igloo_cs_to_bool(const char *str, bool *res) igloo_ATTR_F_WARN_UNUSED_RESULT;
igloo_error_t igloo_cs_to_int(const char *str, int *res) igloo_ATTR_F_WARN_UNUSED_RESULT;
igloo_error_t igloo_cs_to_uint(const char *str, unsigned int *res) igloo_ATTR_F_WARN_UNUSED_RESULT;

/* Convert strings in place to upper or lower case.
 *
 * The *_first() versions convert only the first letter.
 * The conversation may depend on the locale.
 */
igloo_error_t igloo_cs_to_lower(char *str);
igloo_error_t igloo_cs_to_upper(char *str);
igloo_error_t igloo_cs_to_lower_first(char *str);
igloo_error_t igloo_cs_to_upper_first(char *str);

/* Convert a byte string into a string of hexpairs.
 * The result is \0-terminated.
 */
igloo_error_t igloo_cs_to_hex(const void *in, char **res, size_t len) igloo_ATTR_F_WARN_UNUSED_RESULT;

#ifdef __cplusplus
}
#endif

#endif /* ! _LIBIGLOO__CS_H_ */
