/* Copyright (C) 2022       Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef _LIBIGLOO__UUID_H_
#define _LIBIGLOO__UUID_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <igloo/types.h>

/* Creates a new random UUID (v4).
 *
 * This function generates a new random UUID providing a string pool reference.
 * This is the preferred way of working with string based UUIDs as it allows the reference to travel.
 * The reference must be freed with igloo_sp_unref().
 *
 * Parameters:
 *  ref
 *  instance
 *      Reference and instance as used by igloo_sp_replace().
 *
 * See also:
 *  * igloo_sp_replace().
 */
igloo_error_t igloo_uuid_new_random_sp(const char **ref, igloo_ro_t instance) igloo_ATTR_F_WARN_UNUSED_RESULT;

/* Creates a new random UUID (v4).
 *
 * This function generates a new random UUID providing a bare C string.
 * The string must later be freed with free(3).
 *
 * Parameters:
 *  ref
 *      A pointer to a C string. The C string MUST be NULL when this function is called. It will be updated
 *      to a newly created C string containing the UUID. It must be freed with free(3).
 *  instance
 *      The instance to use for the generation process.
 *      The instance may be un-referenced before the provided C string is free(3)ed.
 *
 * See also:
 *  * igloo_uuid_new_random_sp().
 */
igloo_error_t igloo_uuid_new_random_cstr(char **str, igloo_ro_t instance) igloo_ATTR_F_WARN_UNUSED_RESULT;

#ifdef __cplusplus
}
#endif

#endif /* ! _LIBIGLOO__UUID_H_ */
